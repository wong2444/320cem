<?php 
    include_once('Controller/search.php');
?>

<!DOCTYPE html>
<html lang="en">
<script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Your own takeaway menu</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/heroic-features.css" rel="stylesheet">

  </head>

  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="#">TakeAway Menu</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
              <a class="nav-link" href="index.php">Home
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="inputMenu.php">Add Restaurant
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="search.php">Find Restaurant
              </a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <!-- Page Content -->
    <!-- search options -->
    <div class="container">

      <h1 class="display-3" align="center">Find Restaurant Now!</h1>
    
      <!-- Upper(search options) -->
      <form class="form-horizontal" action="search.php" method="get" enctype="multipart/form-data">
        <fieldset>
          <div class="form-group">
              <div class="col-lg-12">
                <input type="text" id="searchKey" name="searchKey" placeholder="Restaurant name, location..."
                    class="form-control input-md" required="required"
                    value="<?php
                      if(!empty($_GET["searchKey"])){
                        echo Search::getSpaceSeperatedKeys($_GET["searchKey"]);
                      }
                    ?>">
              </div>
          </div>

          <div class="form-group">
              <div align="center">
                <input type="submit" id="search" name="search" class="btn btn-success" value="Search">
              </div>
          </div>

        </fieldset>
      </form>
    </div>
    
    <hr />

    <!-- Lower(search results) -->
    <?php
      //if searchKey is null
      if(empty($_GET['searchKey'])){
        echo "<div class=\"jumbotron\">";
        echo "<h2>No detail for searching yet! Please input from above!</h2>";
        echo "</div>";
      }else{
        echo Search::getPackedResult($_GET['searchKey']);

      }
    ?>
  </body>
</html>
