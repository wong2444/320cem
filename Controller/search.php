<?php
  require('Model/Database.php');

  class Search{
    function getPackedResult($searchKeys){
      if(!empty($searchKeys)){
        $validKeys = self::getActualKeys($searchKeys);
        if(!empty($validKeys)){
          $matchRestIds = self::searchAllCat($validKeys);
          if(!empty($matchRestIds)){
            $sortedList = self::sortByPri($matchRestIds, self::getMaxPri($matchRestIds));
            $sortedListWithInfo = self::sortByPriData($sortedList);
            $htmlResult = self::getRestList($sortedListWithInfo);
            return $htmlResult;
          }else{
            $htmlResult = "<div class=\"jumbotron\">";
            $htmlResult = $htmlResult."<h2>No restaurant matches!</h2>";
            $htmlResult = $htmlResult."</div>";
            return $htmlResult;
          }
        }else{
          $htmlResult = "<div class=\"jumbotron\">";
          $htmlResult = $htmlResult."<h2>Invalid Keywords! Please try again!</h2>";
          $htmlResult = $htmlResult."</div>";
          return $htmlResult;
        }
      }
        $htmlResult = "<div class=\"jumbotron\">";
        $htmlResult = $htmlResult."<h2>No detail for searching yet! Please input from above!</h2>";
        $htmlResult = $htmlResult."</div>";
        return $htmlResult;
    }

    function getActualKeys($sk){
        $iSearchKeys = explode(" ", $sk);
        //remove symbols
        $iSearchKeys = preg_replace('/[^\p{L}\p{N}\s]/u', "", $iSearchKeys);

        $i = 0;
        $splitedKeys = array();
        //take all valid keys(not "") and put into $splitedKeys
        foreach ($iSearchKeys as $val){
          if($val != ""){
            $splitedKeys[$i] = $val;
            $i++;
          }
        }
        return $splitedKeys;
    }

    //---col loop, select from database everytime by column of $arrCols
    function searchAllCat($sk){
      $splitedKeys = $sk;
      $keyLength = count($splitedKeys);
      $order = array(); //order[id][priority]

      $arrCols = array("name", "location", "type");
      for ($l = 0; $l < 3; $l++){
        //---loop all keys and create a $sql for query
        $sql = "SELECT ID FROM restaurantdetail".
        " WHERE (".$arrCols[$l]." LIKE '%".$splitedKeys[0]."%'";
        for ($k = 1; $k < $keyLength; $k++){
          $sql = $sql." OR ".$arrCols[$l]." LIKE '%".$splitedKeys[$k]."%'";
        }
        $sql = $sql.") AND isWorking=1";

        //---query
        $result = Database::query($sql);

        //---check if result found
        if($result->num_rows > 0){
          //---result loop
          //put distinct result into $order, and check priority
          //the more the records be found from name/location/type get higher priority
          //default priority=0
          while($row = $result->fetch_assoc()){
            //echo "<br />";
            //echo "cur loop: ".$l;
            //echo " id: ".$row["ID"]; //see if there are results
            if(empty($order)){
              //assign first record found with 0 pri
              $order[0] = array($row["ID"], 0);
            }else{
              //loop through $order to see if the record already exist
              //if exists, add priority/if not exists, push into $order
              for($m = 0; $m < count($order); $m++){
                if($order[$m][0] == $row["ID"]){
                  $order[$m][1]++;
                  break;
                }else if($m+1 == count($order)){
                  $order[$m+1] = array($row["ID"], 0);
                  break;
                }
              }
            }
          }
        }
      }
      return $order;
    }

    //get max priority from splitedsearchkeys
    function getMaxPri($splitedKeysP){
      $max = 0;
      //---get max value of priority for all orders
      foreach($splitedKeysP as $keyP){
        if($keyP[1] > $max){
          $max = $keyP[1];
        }
      }
      return $max;
    }

    //sort the searchkeys by priority and return the sorted list 
    function sortByPri($skp, $mpv){
      $max = $mpv; //max priority value

      $sortedList = array();
      $curPlace = 0;

      for($curMax = $max; $curMax >= 0; $curMax--){
        for($n = 0; $n < count($skp); $n++){
          if($skp[$n][1] == $curMax){
            $sortedList[$curPlace++] = $skp[$n][0];
          }
        }
      }

      return $sortedList;
    }

    function sortByPriData($sl){
      //select corresponding restaurant information
      $sql = " ID='".$sl[0]."' ";
      for ($i = 1; $i < count($sl); $i++){
        $sql = $sql." OR ID='".$sl[$i]."'";
      }
      $sql = "SELECT * FROM restaurantdetail".
      " WHERE ".$sql."";
      $result = Database::query($sql);

      $orderedInfo = array();

      //order the information by priority
      if($result->num_rows > 0){
        $resultCount = $result->num_rows;
        while($row = $result->fetch_assoc()){
          for($p = 0; $p < $resultCount; $p++){
            if($sl[$p] == $row["ID"]){
              $orderedInfo[$p] = $row;
              break;
            }
          }
        }
      }
        return $orderedInfo;
      }

      function getRestList($orderedInfo){

      $strRestList = "";
      if(!empty($orderedInfo)){
        for($q = 0; $q < count($orderedInfo); $q++){
        $href = "restaurant.php?restId=".$orderedInfo[$q]["ID"];
        $strRestList = $strRestList."<a href=\"".$href."\">";
          $strRestList = $strRestList."<div class=\"jumbotron\">";
            $strRestList = $strRestList."<div class=\"container\">";
              $strRestList = $strRestList."<h2>".$orderedInfo[$q]["name"]." (".$orderedInfo[$q]["type"].")</h2>";
            $strRestList = $strRestList."</div>";
            $strRestList = $strRestList."<div class=\"container\">";
              $strRestList = $strRestList."".$orderedInfo[$q]["location"];
            $strRestList = $strRestList. "</div>";
            $strRestList = $strRestList. "<br />";
            $strRestList = $strRestList. "<div class=\"container\">";
              $strRestList = $strRestList. "<table>";
                $strRestList = $strRestList. "<tr>";
                  $strRestList = $strRestList. "<th>Price:</th>";
                  $strRestList = $strRestList. "<td>  ~$".$orderedInfo[$q]["price"]."</td>";
                $strRestList = $strRestList. "</tr>";
                $strRestList = $strRestList. "<tr>";
                  $strRestList = $strRestList. "<th>Opening Hours:</th>";
                  $strRestList = $strRestList. "<td>  ".date("h:ia", strtotime($orderedInfo[$q]["open"]))." - ".date("h:ia", strtotime($orderedInfo[$q]["close"]))."</td>";
                $strRestList = $strRestList. "</tr>";
                $strRestList = $strRestList. "<tr>";
                  $strRestList = $strRestList. "<th>Tel:</th>";
                  $strRestList = $strRestList. "<td>  ".$orderedInfo[$q]["phone"]."</td>";
                $strRestList = $strRestList. "</tr>";
              $strRestList = $strRestList. "</table>";
            $strRestList = $strRestList. "</div>";
            $strRestList = $strRestList. "</div>";
          $strRestList = $strRestList. "</a>";
        }
      }else{ //no records, give out message
        $strRestList = $strRestList. "<div class=\"jumbotron\">";
        $strRestList = $strRestList. "<h2>Sorry! No restaurants found!</h2>";
        $strRestList = $strRestList. "</div>";
      }
      return $strRestList;
    }

    function getSpaceSeperatedKeys($sk){
      $keys = self::getActualKeys($sk);
      $spaceSeperatedKeys = "";
      if(!empty($keys)){
        foreach ($keys as $key){
          $spaceSeperatedKeys = $spaceSeperatedKeys.$key." ";
        }
      }
      return $spaceSeperatedKeys;
    }

  }
?>