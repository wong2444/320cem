<?php
    include_once 'Controller/search.php';

    function test_getActualKeys(){
        $completion = 0;
        echo "<br />";
        $sk = array("1 3 5 7 9",
                    "a e i o u",
                    "a/ %^@$ !??",
                    "a       b",
                    "     ",
                    "hong kong");
        $expectedResults = array(   array(1, 3, 5, 7, 9),
                                    array("a", "e", "i", "o", "u"),
                                    array("a"),
                                    array("a", "b"),
                                    array(),
                                    array("hong", "kong"));
        for($i = 0; $i < count($sk); $i++){
            echo "<br />";
            $curKeys = Search::getActualKeys($sk[$i]);
            echo "cur: ";
            print_r($curKeys);
            echo "<br />";
            echo "expected: ";
            print_r($expectedResults[$i]);
            if($curKeys == $expectedResults[$i]){
                echo "<br />";
                echo "- - Matched - -";
                $completion ++;
            }else{
                echo "<br />";
                echo "- - not match - -";
            }
        }
        
        if($completion == 6){
            echo "<h1>= = = test_getActualKeys : passed! = = =</h1>";
        }else{
            echo "<h1>= = = test_getActualKeys : failed! = = =</h1>";
        }

    }

    function test_getMaxPri(){
        $completion = 0;
        echo "<br />";
        $sk = array(
                array(
                    array(1, 2),
                    array(2, 3),
                    array(3, 1),
                    array(4, 1),
                    array(5, 2),
                    array(6, 2)),
                array(
                    array(5, 2),
                    array(1, 3),
                    array(2, 1),
                    array(4, 1),
                    array(6, 2),
                    array(0, 2))
                );

        $expectedResults = array(3, 3);

        for($i = 0; $i < count($sk); $i++){
                echo "<br />";
                $max = Search::getMaxPri($sk[$i]);
                echo "cur: ".$max;
                echo "<br />";
                echo "expected: ".$expectedResults[$i];
                if($max == $expectedResults[$i]){
                    echo "<br />";
                    echo "- - Matched - -";
                    $completion ++;
                }else{
                    echo "<br />";
                    echo "- - not match - -";
            }
        }
        
        if($completion == 2){
            echo "<h1>= = = test_getMaxPri : passed! = = =</h1>";
        }else{
            echo "<h1>= = = test_getMaxPri : failed! = = =</h1>";
        }
    }

    function test_sortByPri(){
        $completion = 0;
        echo "<br />";
        $sk = array(
                array(
                    array(1, 2),
                    array(2, 3),
                    array(3, 0),
                    array(4, 1),
                    array(5, 2),
                    array(6, 2),
                ),
                array(
                    array(5, 2),
                    array(1, 3),
                    array(2, 1),
                    array(4, 1),
                    array(6, 2),
                    array(0, 2)
                )
            );

        $expectedResults = array(
                            array(
                                2, 1, 5, 6, 4, 3
                            ),
                            array(
                                1, 5, 6, 0, 2, 4
                            )
                        );

        for($i = 0; $i < count($sk); $i++){
                echo "<br />";
                $curSort = Search::sortByPri($sk[$i], 3);
                echo "cur: ";
                print_r($curSort);
                echo "<br />";
                echo "expected: ";
                print_r($expectedResults[$i]);
                if($curSort == $expectedResults[$i]){
                    echo "<br />";
                    echo "- - Matched - -";
                    $completion ++;
                }else{
                    echo "<br />";
                    echo "- - not match - -";
            }
        }
        
        if($completion == 2){
            echo "<h1>= = = test_sortByPri : passed! = = =</h1>";
        }else{
            echo "<h1>= = = test_sortByPri : failed! = = =</h1>";
        }
    }

    function test_getSpaceSeperatedKeys(){
        $completion = 0;
        $sk = array("1 3 5 7 9",
                    "a e i o u",
                    "a/ %^@$ !??",
                    "a       b",
                    "     ",
                    "hong kong");
        $expectedResults = array("1 3 5 7 9 ",
                                    "a e i o u ",
                                    "a ",
                                    "a b ",
                                    "",
                                    "hong kong ");

        for($i = 0; $i <count($sk); $i++){
            echo "<br />";
            $curStr = Search::getSpaceSeperatedKeys($sk[$i]);
            echo "cur: ";
            print_r($curStr);
            echo "<br />";
            echo "expected: ";
            print_r($expectedResults[$i]);

            if($curStr == $expectedResults[$i]){
                echo "<br />";
                echo "- - Matched - -";
                $completion ++;
            }else{
                echo "<br />";
                echo "- - not match - -";
            }
        }
        if($completion == 6){
            echo "<h1>= = = test_getSpaceSeperatedKeys : passed! = = =</h1>";
        }else{
            echo "<h1>= = = test_getSpaceSeperatedKeys : failed! = = =</h1>";
        }
    }

    test_getActualKeys();
    test_getMaxPri();
    test_sortByPri();
    test_getSpaceSeperatedKeys();
?>