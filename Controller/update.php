<?php
    require('Model/Database.php');
    class Update{

        function packedRestInfo($restId){
            $id = self::cleanRestId($restId);
            $sql = self::getSqlStr($id);
            $result = self::requestRestInfo($sql);
            $restInfo = self::getRestInfo($result);
            return $restInfo;
        }

        function cleanRestId($restId){
            $id = $restId;
            $id = preg_replace('/[^\p{L}\p{N}\s]/u', "", $id);
            return $id;
        }

        function getSqlStr($id){
            $sql = "SELECT * FROM restaurantdetail WHERE ID='".$id."'";
            return $sql;
        }

        function requestRestInfo($sql){
            $result = Database::query($sql);
            return $result;
        }

        function getRestInfo($result){
            $restInfo = array();
            if($result->num_rows > 0){
                while($row = $result->fetch_assoc()){
                    $restInfo = $row;
                }
            }
            return $restInfo;
        }

        function requestRestImg($id){
            //query menu image location from database
            $sql = "SELECT * FROM resourseimg WHERE restaurantID='".$id."'";
            $imgResult = Database::query($sql);
            return $imgResult;
        }
    }
?>