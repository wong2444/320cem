<?php
    class Database{
        function query($sql){
            $hostname = "127.0.0.1";
	        $username = "root";
	        $pwd = "";
	        $db = "takeout";
	        $conn = mysqli_connect($hostname, $username, $pwd, $db) or die(mysqli_connect_error());
            
            if($conn->connect_error){
              die("CONNECTION FAILED! ". $conn->connect_error);
            }else{
              $result = $conn->query($sql);
              return $result;
            }
        }
    }
?>