-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- 主機: 127.0.0.1
-- 產生時間： 2018 年 12 月 24 日 10:18
-- 伺服器版本: 10.1.30-MariaDB
-- PHP 版本： 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫： `takeout`
--

-- --------------------------------------------------------

--
-- 資料表結構 `resourseimg`
--

CREATE TABLE `resourseimg` (
  `ID` int(12) NOT NULL,
  `restaurantID` int(11) NOT NULL,
  `image` varchar(55) NOT NULL,
  `meaning` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- 資料表的匯出資料 `resourseimg`
--

INSERT INTO `resourseimg` (`ID`, `restaurantID`, `image`, `meaning`) VALUES
(24, 20, 'uploads/20181224073611726.jpg', NULL),
(25, 21, 'uploads/20181224081139410.jpg', NULL),
(26, 22, 'uploads/20181224081441959.jpg', NULL),
(27, 23, 'uploads/20181224081753870.jpg', NULL),
(28, 24, 'uploads/20181224090939281.jpg', NULL),
(29, 19, 'uploads/20181224091511274.jpg', NULL),
(36, 26, 'uploads/20181224092414293.jpg', NULL),
(37, 26, 'uploads/20181224092414342.png', NULL),
(38, 27, 'uploads/20181224092532546.jpg', NULL),
(39, 27, 'uploads/20181224092532681.png', NULL),
(40, 24, 'uploads/20181224092624966.jpg', NULL),
(41, 24, 'uploads/20181224092624168.png', NULL),
(42, 28, 'uploads/20181224092646668.jpg', NULL),
(43, 28, 'uploads/20181224092646607.png', NULL),
(44, 29, 'uploads/20181224092849834.jpg', NULL),
(45, 29, 'uploads/20181224092849874.png', NULL),
(46, 30, 'uploads/20181224092937676.jpg', NULL),
(47, 30, 'uploads/20181224092937382.png', NULL),
(49, 25, 'uploads/20181224093723714.jpg', NULL),
(51, 31, 'uploads/20181224094048122.jpg', NULL),
(52, 31, 'uploads/20181224094048883.jpg', NULL),
(56, 32, 'uploads/20181224095258567.png', NULL);

-- --------------------------------------------------------

--
-- 資料表結構 `restaurantdetail`
--

CREATE TABLE `restaurantdetail` (
  `ID` int(10) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `LocationX` double(10,6) DEFAULT NULL,
  `LocationY` double(10,6) DEFAULT NULL,
  `type` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` int(20) DEFAULT NULL,
  `open` time(6) DEFAULT NULL,
  `close` time(6) DEFAULT NULL,
  `price` double(10,2) DEFAULT NULL,
  `edittime` timestamp(6) NULL DEFAULT CURRENT_TIMESTAMP(6),
  `isWorking` tinyint(1) NOT NULL DEFAULT '1',
  `Distance` float(14,7) DEFAULT '9999999.0000000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `restaurantdetail`
--

INSERT INTO `restaurantdetail` (`ID`, `name`, `location`, `LocationX`, `LocationY`, `type`, `phone`, `open`, `close`, `price`, `edittime`, `isWorking`, `Distance`) VALUES
(19, 'mcdonalds', 'Hong Kong, fotan', 22.398571, 114.192943, 'fast food', 55665566, '01:00:00.000000', '12:59:00.000000', 31.00, '2018-12-20 11:03:17.038780', 1, 12.7048788),
(21, 'Kabushiki-gaisha Saizeriya', 'Shop 402B, 4/F, Siu Sai Wan Plaza, 10 Siu Sai Wan Road.', 22.267810, 114.236078, 'itanly', 23643887, '10:00:00.000000', '22:00:00.000000', 101.00, '2018-12-24 07:11:39.476816', 1, 4.2457452),
(22, 'KFC', 'Cheung Sha Wan Plaza, Cheung Sha Wan Road, Lai Chi Kok, Hong Kong', 22.337108, 114.148727, 'fast food', 26785499, '07:00:00.000000', '10:00:00.000000', 50.00, '2018-12-24 07:14:41.354906', 1, 11.8337021),
(23, 'MX', 'Hong Kong, Tseung Kwan O, Sheung Ning Road, TKO Gateway', 22.317200, 114.265948, 'fast food', 25276689, '07:00:00.000000', '10:30:00.000000', 58.00, '2018-12-24 07:17:53.121071', 1, 2.0493245),
(32, 'Kfc', 'KFC, Shan Mei Street, Fo Tan, Hong Kong', 22.397084, 114.193515, 'fast food', 36436419, '07:00:00.000000', '10:00:00.000000', 50.00, '2018-12-24 08:50:42.993392', 1, 12.5330257);

-- --------------------------------------------------------

--
-- 資料表結構 `user`
--

CREATE TABLE `user` (
  `uID` int(11) NOT NULL,
  `name` varchar(12) NOT NULL,
  `password` varchar(30) NOT NULL,
  `text` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- 已匯出資料表的索引
--

--
-- 資料表索引 `resourseimg`
--
ALTER TABLE `resourseimg`
  ADD PRIMARY KEY (`ID`);

--
-- 資料表索引 `restaurantdetail`
--
ALTER TABLE `restaurantdetail`
  ADD PRIMARY KEY (`ID`);

--
-- 資料表索引 `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`uID`);

--
-- 在匯出的資料表使用 AUTO_INCREMENT
--

--
-- 使用資料表 AUTO_INCREMENT `resourseimg`
--
ALTER TABLE `resourseimg`
  MODIFY `ID` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- 使用資料表 AUTO_INCREMENT `restaurantdetail`
--
ALTER TABLE `restaurantdetail`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- 使用資料表 AUTO_INCREMENT `user`
--
ALTER TABLE `user`
  MODIFY `uID` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
